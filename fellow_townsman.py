# coding: utf-8

from flask import Flask, jsonify

app = Flask(__name__)


@app.route("/fellow_townsman/circle_details", methods=["POST"])
def circle_details():
    response = {
        "is_ios": False,
        "timestamp": "1513909094",
        "err_msg": "OK",
        "result": {
            "user_count": 231456,
            "is_admin": 1,
            "circle_id": "1234",
            "name": "ret",
            "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
        },
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/circle_list", methods=["GET"])
def circle_list():
    response = {
        "is_ios": False,
        "timestamp": "1513909293",
        "err_msg": "OK",
        "result": {
            "circle_list": [
                {
                    "user_count": 231456,
                    "name": "河南老乡圈",
                    "circle_id": "123",
                    "background_img": "http://p1bbcze4r.bkt.clouddn.com/bg1.png",
                    "tweet_user": {
                        "user_id": "30006648",
                        "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                    },
                    "open": 1,
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                },
                {
                    "user_count": 2314,
                    "name": "天津老乡圈",
                    "circle_id": "123",
                    "background_img": "http://p1bbcze4r.bkt.clouddn.com/bg2.png",
                    "tweet_user": {
                        "user_id": "30006648",
                        "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                    },
                    "open": 1,
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                },
                {
                    "user_count": 2314,
                    "name": "天津老乡圈",
                    "circle_id": "123",
                    "background_img": "http://p1bbcze4r.bkt.clouddn.com/bg3.png",
                    "tweet_user": {
                        "user_id": "30006648",
                        "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                    },
                    "open": 0,
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                }
            ]
        },
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/nearby_fellow_townsman", methods=["POST"])
def nearby_fellow_townsman():
    response = {
        "is_ios": False,
        "timestamp": "1513909379",
        "err_msg": "OK",
        "result": {
            "start_num": 20,
            "fellow_townsman_count": 24,
            "user_list": [
                {
                    "distance": "32.3km",
                    "user_id": "1491720330037597731",
                    "nick_name": "用户名字1",
                    "gender": "male",
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "hometowns": "北京市/朝阳区",
                    "relationship_description": "你的朋友他他他他关注了她",
                    "occupation": "销售"
                },
                {
                    "distance": "3.3km",
                    "user_id": "1491720330037597731",
                    "nick_name": "用户名字2",
                    "gender": "male",
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "hometowns": "北京市/朝阳区",
                    "relationship_description": "你的朋友他他他他关注了她",
                    "occupation": "销售"
                },
                {
                    "distance": "321.3km",
                    "user_id": "1491720330037597731",
                    "nick_name": "用户名字3",
                    "gender": "male",
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "hometowns": "北京市/朝阳区",
                    "relationship_description": "你的朋友他他他他关注了她",
                    "occupation": "销售"
                },
                {
                    "distance": "312.3km",
                    "user_id": "30006648",
                    "nick_name": "用户名字4",
                    "gender": "male",
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "hometowns": "北京市/朝阳区",
                    "relationship_description": "你的朋友他他他他关注了她",
                    "occupation": "销售"
                }
            ],
            "without_data": 1
        },
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/tweet_list", methods=["POST"])
def tweet_list():
    response = {
        "is_ios": False,
        "timestamp": "1513909414",
        "err_msg": "OK",
        "result": {
            "start_num": 20,
            "tweet_list": [
                {
                    "tweet": {
                        "liked": 0,
                        "pictures": [
                            "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                        ],
                        "origin_pictures": [
                            "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                        ],
                        "create_at_timestamp": 1513826247910,
                        "content": "用户发布的时间",
                        "id": "123",
                        "likes_count": 25
                    },
                    "user": {
                        "distance": "32.3km",
                        "user_id": "12",
                        "nick_name": "名字",
                        "gender": "male",
                        "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                        "hometowns": "北京市/朝阳区"
                    }
                },
                {
                    "tweet": {
                        "liked": 0,
                        "pictures": [
                            "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                        ],
                        "origin_pictures": [
                            "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                        ],
                        "create_at_timestamp": 1513826247910,
                        "content": "用户发布的时间",
                        "id": "123",
                        "likes_count": 25
                    },
                    "user": {
                        "distance": "3.3km",
                        "user_id": "12",
                        "nick_name": "名字2",
                        "gender": "male",
                        "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                        "hometowns": "北京市/朝阳区"
                    }
                },
                {
                    "tweet": {
                        "liked": 0,
                        "pictures": [
                            "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                        ],
                        "origin_pictures": [
                            "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg"
                        ],
                        "create_at_timestamp": 1513826247910,
                        "content": "用户发布的时间",
                        "id": "123",
                        "likes_count": 25
                    },
                    "user": {
                        "distance": "332.3km",
                        "user_id": "12",
                        "nick_name": "名字3",
                        "gender": "male",
                        "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                        "hometowns": "北京市/朝阳区"
                    }
                }
            ],
            "without_data": 1
        },
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/tweet_add", methods=["POST"])
def tweet_add():
    response = {
        "is_ios": False,
        "timestamp": "1513909467",
        "err_msg": "OK",
        "result": None,
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/tweet_delete", methods=["POST"])
def tweet_delete():
    response = {
        "is_ios": False,
        "timestamp": "1513909500",
        "err_msg": "OK",
        "result": None,
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/tweet_like", methods=["POST"])
def tweet_like():
    response = {
        "is_ios": False,
        "timestamp": "1513909527",
        "err_msg": "OK",
        "result": None,
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/circle_groups", methods=["POST"])
def circle_groups():
    response = {
        "is_ios": False,
        "timestamp": "1513909550",
        "err_msg": "OK",
        "result": {
            "group_list": [
                {
                    "group_id": "1",
                    "group_user_count": 8989,
                    "group_name": "1",
                    "group_desc": "群组的描述1"
                },
                {
                    "group_id": "2",
                    "group_user_count": 8989,
                    "group_name": "2",
                    "group_desc": "群组的描述2"
                },
                {
                    "group_id": "3",
                    "group_user_count": 8989,
                    "group_name": "3",
                    "group_desc": "群组的描述3"
                }
            ]
        },
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


@app.route("/fellow_townsman/circle_user_rank", methods=["POST"])
def circle_user_rank():
    response = {
        "is_ios": False,
        "timestamp": "1494233311",
        "err_msg": "OK",
        "result": {
            "user_info_list": [
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",  # 用户头像
                    "nick_name": "大风",
                    "flower_count": 23424,
                    "rank": 1  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风2",
                    "flower_count": 2,
                    "rank": 2  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风3",
                    "flower_count": 424,
                    "rank": 3  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风4",
                    "flower_count": 23424,
                    "rank": 4  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风5",
                    "flower_count": 23424,
                    "rank": 5  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风6",
                    "flower_count": 23424,
                    "rank": 6  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风7",
                    "flower_count": 23424,
                    "rank": 7  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风8",
                    "flower_count": 23424,
                    "rank": 8  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风9",
                    "flower_count": 23424,
                    "rank": 9  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风10",
                    "flower_count": 23424,
                    "rank": 10  # 排名等级
                },
                {
                    "user_id": "1491720330037597731",  # 加密的ID
                    "avatar": "http://cootek-dialer-download.oss-cn-hangzhou.aliyuncs.com/social_head/head/14144293324179553597.jpg",
                    "nick_name": "大风11",
                    "flower_count": 23424,
                    "rank": 11  # 排名等级
                },
            ]
        },
        "req_id": 0,
        "result_code": 2000
    }
    return jsonify(response)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9999)
